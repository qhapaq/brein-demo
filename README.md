# Primero
Agregar la plataforma
```sh
tns platform add android
```
# Firebase
Para que funcione firebase es necesario hacer lo siguiente.
Agregar platform/android/app/build.gradle
```sh
dependencies {
    ...
    implementation 'com.google.firebase:firebase-core:16.0.6'
    implementation 'com.google.firebase:firebase-auth:16.1.0'
}
```
y al final
```sh
apply plugin: 'com.google.gms.google-services'
```

> Espero no olvidarme algo mas

para ejecutar
```sh
tns run android
```