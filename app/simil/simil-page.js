const SimilViewModel = require("./simil-view-model");
const similViewModel = new SimilViewModel();

exports.pageLoaded = function (args) {
    const page = args.object;
    similViewModel.data = page.navigationContext;
    page.bindingContext = similViewModel;
}
