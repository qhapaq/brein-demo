const observableModule = require("data/observable");
const topmost = require("ui/frame").topmost;
var utilsModule = require("tns-core-modules/utils/utils");

function SimilViewModel() {
  const viewModel = observableModule.fromObject({
    data: {},
    onTapToDetail(args) {
      const data = args.object.page.bindingContext.data;
      topmost().navigate({
        moduleName: "./detail/detail-page",
        context: {
          list: data.list,
          item: args.object.project
        }
      });
    },
    openBrowser(args) {
      utilsModule.openUrl(args.object.text)
    },
    goBack() {
      topmost().goBack();
    },
  });

  return viewModel;
}

module.exports = SimilViewModel;
