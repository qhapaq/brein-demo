const observableModule = require("data/observable");
const topmost = require("ui/frame").topmost;
const utilsModule = require("tns-core-modules/utils/utils");
const appData = require("../app-data");
const firebase = require("nativescript-plugin-firebase");

function DetailViewModel() {
  const viewModel = observableModule.fromObject({
    data: {},
    similes: [],
    similesLength: 0,
    isFavorite: false,
    waitFavorite: false,
    goBack() {
      topmost().goBack();
    },
    openBrowser(args) {
      utilsModule.openUrl(args.object.text)
    },
    prepareData() {

      // search similes
      const list = this.data.list;
      const item = this.data.item;
      const result = list.map(itex => {
        const str1 = `${item.nombre.toLowerCase()} ${item.industria.toLowerCase()} ${item.solucion.toLowerCase()} ${item.pais.toLowerCase()} ${item.fecha.toLowerCase()} ${item.universidad.toLowerCase()}`;
        const str2 = `${itex.nombre.toLowerCase()} ${itex.industria.toLowerCase()} ${itex.solucion.toLowerCase()} ${itex.pais.toLowerCase()} ${itex.fecha.toLowerCase()} ${itex.universidad.toLowerCase()}`;
        itex.similar = Math.ceil(this.calcSimilarity(str1, str2) * 100) ;
        return itex;
      })
      .filter(itex => itex.similar > 50 && itex.nombre !== item.nombre )
      .sort((prev, next) => {
        return next.similar - prev.similar;
      });
      this.similes = result;
      this.similesLength = result.length;

      // search isFavorite
      this.isFavorite = appData.favsName.indexOf(this.data.item.nombre) >= 0;
    },
    calcSimilarity(s1, s2) {
      var longer = s1;
      var shorter = s2;
      if (s1.length < s2.length) {
        longer = s2;
        shorter = s1;
      }
      var longerLength = longer.length;
      if (longerLength == 0) {
        return 1.0;
      }
      return (longerLength - this.calcDistance(longer, shorter)) / parseFloat(longerLength);
    },
    calcDistance(s1, s2) {
      s1 = s1.toLowerCase();
      s2 = s2.toLowerCase();

      var costs = new Array();
      for (var i = 0; i <= s1.length; i++) {
        var lastValue = i;
        for (var j = 0; j <= s2.length; j++) {
          if (i == 0)
            costs[j] = j;
          else {
            if (j > 0) {
              var newValue = costs[j - 1];
              if (s1.charAt(i - 1) != s2.charAt(j - 1))
                newValue = Math.min(Math.min(newValue, lastValue),
                  costs[j]) + 1;
              costs[j - 1] = lastValue;
              lastValue = newValue;
            }
          }
        }
        if (i > 0)
          costs[s2.length] = lastValue;
      }
      return costs[s2.length];
    },
    onTapToSimil() {
      topmost().navigate({
        moduleName: "./simil/simil-page",
        context: {
          similes: this.similes,
          list: this.data.list,
          name: this.data.item.nombre
        }
      });
    },
    onTapToFilter(args) {
      topmost().navigate({
        moduleName: "./list/list-page",
        context: {
          list: this.data.list,
          filterType: {
            label: args.object.typeLabel,
            value: args.object.typeValue,
            caption: args.object.typeCaption
          }
        }
      });
    },
    onTapFavorite(args) {
      if (this.waitFavorite) return;
      this.waitFavorite = true;
      const currentName = this.data.item.nombre;
      if (this.isFavorite) {
        this.isFavorite = false;
        appData.favs = appData.favs.filter(itx => itx.nombre !== currentName);
        appData.favsName = appData.favsName.filter(name => name !== currentName && name !== null);
      } else {
        this.isFavorite = true;
        appData.favs.push(this.data.list.find(itx => itx.nombre === currentName));
        appData.favsName.push(currentName);
      }
      firebase.setValue('/favs/' + appData.user.uid, appData.favsName).then(
        result => {
          this.waitFavorite = false;
        },
        error => {
          this.waitFavorite = false;
        }
      );

    }
  });

  return viewModel;
}

module.exports = DetailViewModel;
