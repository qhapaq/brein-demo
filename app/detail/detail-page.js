const frameModule = require("ui/frame");
const DetailViewModel = require("./detail-view-model");
const detailViewModel = new DetailViewModel();

exports.pageLoaded = function (args) {
    const page = args.object;
    detailViewModel.data = page.navigationContext;
    detailViewModel.prepareData();
    page.bindingContext = detailViewModel;
}
