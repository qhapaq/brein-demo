const observableModule = require("data/observable");
const topmost = require("ui/frame").topmost;
var utilsModule = require("tns-core-modules/utils/utils");

function ListViewModel() {
  const viewModel = observableModule.fromObject({
    dataOriginal: {},
    dataList: [],
    filter: {},
    onTapToDetail(args) {
      
      const list = this.dataOriginal ? this.dataOriginal.list : args.object.page.bindingContext.dataOriginal.list;
      topmost().navigate({
        moduleName: "./detail/detail-page",
        context: {
          list: list,
          item: args.object.project
        }
      });
    },
    openBrowser(args) {
      utilsModule.openUrl(args.object.text)
    },
    goBack() {
      topmost().goBack();
    },
  });

  return viewModel;
}

module.exports = ListViewModel;
