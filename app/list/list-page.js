const ListViewModel = require("./list-view-model");
const listViewModel = new ListViewModel();

exports.pageLoaded = function (args) {
    const page = args.object;
    const data = page.navigationContext;
    const filterType = data.filterType;
    listViewModel.dataOriginal = data;
    let dataList = [];
    if (filterType) {
        dataList = data.list.filter(item => item[filterType.label] === filterType.value);
    } else {
        dataList = data.list;
    }
    listViewModel.dataList = dataList;
    listViewModel.filter = filterType;
    page.bindingContext = listViewModel;
}
