const application = require("tns-core-modules/application");

const firebase = require("nativescript-plugin-firebase");
const topmost = require("ui/frame").topmost;
const appData = require("./app-data");

console.log("==== INIT");

firebase.init({
  persist: false,
  onAuthStateChanged: (data) => {
    console.log("==== onAuthStateChanged", data.loggedIn);
    if (data.loggedIn) {
      appData.user = data.user;
      topmost().navigate({
        moduleName: "./home/home-page",
        context: data.user
      });
      console.log('==== onAuthStateChanged login');
    } else {
      console.log('==== onAuthStateChanged no login')
      topmost().navigate({
        moduleName: "./login/login-page",
        context: {
          forceLogin: true
        }
      });
    }
  }
}).then(
  () => {
    console.log('==== firebase.init ok')

    // firebase.push();
  },
  error => {
    console.log('==== firebase.init Error')
  }
);

application.run({ moduleName: "app-root" });
