const SearchViewModel = require("./search-view-model");
const searchViewModel = new SearchViewModel();
const platformModule = require("tns-core-modules/platform");
const appData = require("../app-data");

exports.pageLoaded = function (args) {
  const page = args.object;
  const data = page.navigationContext;
  if (data.type === "list") {
    searchViewModel.dataList = data.list;
  }
  searchViewModel.dataListOriginal = data.list;
  page.bindingContext = searchViewModel;
}
exports.loadedSearchBar = function (args) {
  if (platformModule.isAndroid && !appData.isSearch) {
    args.object.android.clearFocus();
  }
}