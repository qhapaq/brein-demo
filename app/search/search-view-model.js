const observableModule = require("data/observable");
const topmost = require("ui/frame").topmost;
const utilsModule = require("tns-core-modules/utils/utils");

function SearchViewModel() {
  const viewModel = observableModule.fromObject({
    dataListOriginal: [],
    dataList: [],
    typex: '-',
    onSearch(args) {
      const search = args.object.text.toLowerCase();
      if (search.length>1) {
        this.dataList = this.dataListOriginal.filter(item => 
          item.nombre.toLowerCase().indexOf(search) >= 0 ||
          item.industria.toLowerCase().indexOf(search) >= 0 ||
          item.solucion.toLowerCase().indexOf(search) >= 0 ||
          item.pais.toLowerCase().indexOf(search) >= 0 ||
          item.fecha.toLowerCase().indexOf(search) >= 0 ||
          item.www.toLowerCase().indexOf(search) >= 0 ||
          item.universidad.toLowerCase().indexOf(search) >= 0 
        );
      } else {
        this.dataList = this.dataListOriginal;
      }
    },
    onTapToDetail(args) {
      const list = this.dataListOriginal ? this.dataListOriginal : args.object.page.bindingContext.dataListOriginal;
      topmost().navigate({
        moduleName: "./detail/detail-page",
        context: {
          list: list,
          item: args.object.project
        }
      });
    },
    openBrowser(args) {
      utilsModule.openUrl(args.object.text)
    },
    goBack() {
      topmost().goBack();
    },
  });

  return viewModel;
}

module.exports = SearchViewModel;
