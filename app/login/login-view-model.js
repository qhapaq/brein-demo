const observableModule = require("data/observable");
const topmost = require("ui/frame").topmost;
const firebase = require("nativescript-plugin-firebase");


function LoginViewModel() {
  const viewModel = observableModule.fromObject({
      email: "",
      password: "",
      isLoading: true,
      login() {
        if (this.email.trim() === "" || this.password.trim() === "") {
          alert("Por favor ingrese sus credenciales de acceso.");
          return;
        }

        firebase.login(
          {
            type: firebase.LoginType.PASSWORD,
            passwordOptions: {
              email: this.email,
              password: this.password
            }
          })
          .then(result => {
            topmost().navigate({
              moduleName: "./home/home-page",
              clearHistory: true
            });
          })
          .catch(error => {
            alert("Usuario y/o clave incorrecta, verifique los datos y vuelve a intentar");
          });
  
      },
      forceLogin() {
        this.isLoading = false;
      }
  });

  return viewModel;
}

module.exports = LoginViewModel;

