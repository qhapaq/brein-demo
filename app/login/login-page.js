const LoginViewModel = require("./login-view-model");
const loginViewModel = new LoginViewModel();

exports.pageLoaded = function (args) {
  const page = args.object;
  const context = page.navigationContext;
  if (context && context.forceLogin) {
    loginViewModel.forceLogin();
  }
  page.bindingContext = loginViewModel;
}
