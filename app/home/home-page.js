const HomeViewModel = require("./home-view-model");
const homeViewModel = new HomeViewModel();

exports.pageLoaded = function (args) {
    const page = args.object;
    homeViewModel.user = page.navigationContext;
    homeViewModel.getData();
    page.bindingContext = homeViewModel;
}
