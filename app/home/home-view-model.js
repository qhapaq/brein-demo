const observableModule = require("data/observable");
const topmost = require("ui/frame").topmost;
const utilsModule = require("tns-core-modules/utils/utils");
const firebase = require("nativescript-plugin-firebase");
const appData = require("../app-data");

function HomeViewModel() {
  const viewModel = observableModule.fromObject({
    isLoading: true,
    dataList: [],
    totalProjects: 0,
    newDataList: [],
    typesProjects: {},
    favs: [],
    user: {},
    getName(name, value) {
      if (name === "1") return "nombre";
      else if (name === "2") return "industria";
      else if (name === "3") return "solucion";
      else if (name === "4") return "pais";
      else if (name === "5") return "fecha";
      else if (name === "6") return "www";
      else return "universidad";
    },
    getData() {

      // get from google sheets
      
      fetch('https://spreadsheets.google.com/feeds/cells/1KSe1V27k_RY4cE7gIcUJD0Kaca9uRyQKNdoTLLVisj0/1/public/full?alt=json')
      .then((response) => response.json())
      .then((r) => {
        const dataListNew = {};
        const typesProjects = {};
        const data = r.feed.entry;
        const total = data.length;
        
        for(let i = 0; i < total; i += 1) {
          const itemFrom = data[i]['gs$cell'];
          if (parseFloat(itemFrom.row)>1){
            const itemList = dataListNew[itemFrom.row];
            const name = this.getName(itemFrom.col);
            if (itemList) {
              itemList[name] = itemFrom.inputValue;
            } else {
              dataListNew[itemFrom.row] = {
                nombre: '',
                industria: '',
                solucion: '',
                pais: '',
                fecha: '',
                www: '',
                universidad: ''
              }
              dataListNew[itemFrom.row][name] = itemFrom.inputValue;
            }

            // agrupamos tipos
            if ('industria,solucion,pais,universidad'.indexOf(name)>=0) {
              const value1 = typesProjects[name];
              if (value1) {
                const value2 = value1[itemFrom.inputValue];
                if (value2) {
                  typesProjects[name][itemFrom.inputValue] += 1;
                } else {
                  typesProjects[name][itemFrom.inputValue] = 1;
                }
              } else {
                typesProjects[name] = {[itemFrom.inputValue]: 1};
              }
            }    
          }
        }
        const tmp = Object.keys(dataListNew).map(function(item) { 
          const itemTmp = dataListNew[item];
          // this.getTipos(itemTmp.col, itemFrom.inputValue)
          itemTmp.prefijo = itemTmp.nombre.substring(0,1).toUpperCase();
          itemTmp.color = '#'+'0123456789abcdef'.split('').map(function(v,i,a){ return i>5 ? null : a[Math.floor(Math.random()*16)] }).join('');
          return itemTmp;
        }).sort((prev, next) => {
          let prevDate = prev.fecha.split(/[\/-]/);
          let nextDate = next.fecha.split(/[\/-]/);

          prevDate = new Date([prevDate[1],prevDate[0], prevDate[2]].join('/')).getTime();
          nextDate = new Date([nextDate[1],nextDate[0], nextDate[2]].join('/')).getTime();
          return nextDate - prevDate;
        });
        
        this.newDataList = tmp.slice(0,3);
        this.totalProjects = tmp.length;
        this.dataList = tmp;
        this.isLoading = false;

        // totalizamos tipos
        Object.keys(typesProjects).forEach(k => {
          const types = typesProjects[k];
          const keys = Object.keys(types);
          typesProjects[k] = {
            total: keys.length,
            topName: keys.reduce((a,b) =>  types[a] > types[b] ? a : b),
            topX: Math.max.apply(Math, keys.map(itm => types[itm])),
            // detail: types
          }
        });

        this.typesProjects = typesProjects;
        
        // get from firebase favs
        firebase.getValue('/favs/' + appData.user.uid).then(
          result => {
            const dataList = this.dataList;
            const favsName = result.value;
            const favs = favsName
              .map(name => dataList.find(itx => itx.nombre === name))
              .filter(itx => itx !== undefined);
            appData.favs = favs;
            appData.favsName = favsName;
            this.favs = favs;
          },
          error => {
            console.log("=========> getValue favs: " + error);
          }
        );

      }).catch((err) => {
      });

    },
    onTapToDetail(args) {
      const list = this.dataList ? this.dataList : args.object.page.bindingContext.dataList;
      topmost().navigate({
        moduleName: "./detail/detail-page",
        context: {
          list: list,
          item: args.object.project
        }
      });
    },
    onTapToList(args){
      topmost().navigate({
        moduleName: "./list/list-page",
        context: this.dataList
      });
    },
    onTapToSearch(args){
      appData.isSearch = args.object.typex === "search";
      topmost().navigate({
        moduleName: "./search/search-page",
        context: {
          list: this.dataList,
          type: args.object.typex,
        }
      });
    },
    onTapToFilter(args) {
      const list = this.dataList ? this.dataList : args.object.page.bindingContext.dataList;
      topmost().navigate({
        moduleName: "./list/list-page",
        context: {
          list: list,
          filterType: {
            label: args.object.typeLabel,
            value: args.object.typeValue,
            caption: args.object.typeCaption
          }
        }
      });
    },
    openBrowser(args) {
      utilsModule.openUrl(args.object.text)
    },
    onTapLogout() {
      firebase.logout();
    }
  });

  return viewModel;
}

module.exports = HomeViewModel;
